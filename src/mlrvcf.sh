#!/bin/bash


# Remove header from VCF file and set first line to uncommented column names
drop_header() {
   egrep -v '^##|^X|^Y' \
   | sed 's/#CHROM/CHROM/'
}


# Convert VCF file to DKVP format with semicolon field separators
vcf2dkvp() {
  mlr --icsv --irs lf --ifs tab --odkvp --ofs semicolon \
    put '$INFO = ".".";".$INFO'
}


# Extract marker positions
# By default positions are returned for reference GRCh37
# Add argument 'HG38' to return positions for reference GRCh38
extract_marker_position() {
  local reference="$1"
  
  if [ "$reference" == "HG38" ]; then
    local field="HG38POS"
  else
    local field="POS"
  fi
  
  mlr --idkvp --ips equals --ifs semicolon \
    --ocsvlite \
    cut -f ID,CHROM,"$field" then \
    reorder -f ID,CHROM,"$field" then \
    rename -r "HG38,"
}


# Extract a nested information field
# First argument must be a valid nested field
extract_nested_field() {
  local field="$1"
  
  local valid=0
  local fields=(
    'ROADMAP_CHROMHMM_15STATE'
    'ROADMAP_CHROMHMM_25STATE'
    'ROADMAP_HISTONE_MARK_PEAKS'
    'ROADMAP_DNASE'
  )
  
  for f in "${fields[@]}"; do
    if [ "$f" == "$field" ]; then
      valid=$(( valid + 1 ))
    fi
  done
  
  if test $valid -eq 0 ; then
    echo "$field is not a valid choice for extract_nested_field()"
    exit 1;
  fi

  mlr --idkvp --ips equals --ifs semicolon \
    --ocsvlite \
  	cut -f ID,"$field" then \
  	having-fields --which-are ID,"$field" then \
  	nest --explode --values --across-records -f "$field" --nested-fs comma then \
  	nest --explode --values --across-fields -f "$field" --nested-fs pipe then \
  	rename "$field"_1,eid,"$field"_2,feature
}


# Extract nearest genes
# First argument must be REFSEQ or GENCODE
extract_nearest_gene() {
  local field="NEAREST_$1"
  
  local valid=0
  local fields=(
    'NEAREST_GENCODE'
    'NEAREST_REFSEQ'
  )
  
  for f in "${fields[@]}"; do
    if [ "$f" == "$field" ]; then
      valid=$(( valid + 1 ))
    fi
  done
  
  if test $valid -eq 0 ; then
    echo "$field is not a valid choice for extract_nearest_gene()"
    exit 1;
  fi

  mlr --idkvp --ips equals --ifs semicolon \
    --ocsvlite \
  	cut -f ID,"$field" then \
    having-fields --which-are ID,"$field" then \
    nest --explode --values --across-fields -f "$field" --nested-fs comma then \
    rename "$field"_1,DISTANCE,"$field"_2,DIRECTION,"$field"_3,GENEID,"$field"_4,SYMBOL then \
    reorder -f ID,GENEID,SYMBOL,DIRECTION,DISTANCE
}
