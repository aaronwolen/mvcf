# mlrvcf

A VCF parser based on [miller][1].

Currently specialized for parsing the VCF file provided by [Haploreg][2]. 


1: http://johnkerl.org/miller/doc/index.html
2: http://www.broadinstitute.org/mammals/haploreg/haploreg.php