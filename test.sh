#!/bin/bash
source src/mlrvcf.sh

# A subset of the Haploreg v4.1 VCF file
vcf=data/temp.vcf
dkvp=temp.dkvp

if [ ! -d "results" ]; then mkdir results; fi

# Create dkvp version of VCF file
cat "$vcf" \
  | drop_header \
  | vcf2dkvp > "$dkvp"
  

# Marker positions
cat "$dkvp" \
  | extract_marker_position > results/pos-hg37.csv

cat "$dkvp" \
  | extract_marker_position HG38 > results/pos-hg38.csv


# Nearest genes
cat "$dkvp" \
  | extract_nearest_gene REFSEQ > results/gene-refseq.csv
  
cat "$dkvp" \
  | extract_nearest_gene GENCODE > results/gene-gencode.csv


# Nested fields
cat "$dkvp" \
  | extract_nested_field ROADMAP_CHROMHMM_15STATE > results/roadmap-chrom15.csv

cat "$dkvp" \
  | extract_nested_field ROADMAP_DNASE > results/roadmap-dnase.csv